from flask import Flask, render_template, request

app = Flask(__name__)

# Main page
@app.route('/', methods = ['GET', 'POST'])
def index():
    return render_template("index.html")

# Prices
@app.route('/Cenik', methods = ['GET', 'POST'])
def cenik():
    return render_template("Cenik.html")

# Services
@app.route('/Sluzby', methods = ['GET', 'POST'])
def sluzby():
    return render_template("Sluzby.html")

# About 
@app.route('/Aneta', methods = ['GET', 'POST'])
def about():
    return render_template("Aneta.html")
